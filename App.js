import Ionicons from '@expo/vector-icons/Ionicons';

// Components
import HomeComponent from './components/home/Home';
import LocationComponent from './components/location/Location';
import MyLibraryComponent from './components/my-library/My-Library';
import Products from './components/products/Products';
import SearchComponent from './components/Search/Search';

// Navigation:
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// Redux
import { Provider } from 'react-redux';
import { store } from './store';

const Tab = createBottomTabNavigator();
// Posso anche fare così, se voglio prendermeli separatamene:
// const { Navigator, Tab } = createBottomTabNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarActiveTintColor: 'green',
            tabBarIcon: ({ color, focused, size }) => {
              let iconName;

              switch(route.name) {
                case 'home': 
                  iconName = focused ? 'ios-home' : 'ios-home-outline';
                  break;
                case 'search':
                  iconName = focused ? 'ios-search' : 'ios-search-outline';
                  break;
                case 'my-library':
                  iconName = focused ? 'ios-albums' : 'ios-albums-outline';
                  break;
              }

              return <Ionicons name={iconName} size={size} color={color} />
            },
            tabBarInactiveTintColor: 'blue',
          })}
        >
          <Tab.Screen name='home' component={HomeComponent} options={{ title: 'Home' }} />
          <Tab.Screen name='search' component={SearchComponent} options={{ title: 'Search' }} />
          <Tab.Screen name='my-library' component={MyLibraryComponent} options={{ title: 'Library' }} />
          <Tab.Screen name='products' component={Products} options={{ title: 'Products' }} />
          <Tab.Screen name='map' component={LocationComponent} options={{ title: 'Location' }} />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}