// Un solo reducer per file
import { ADD_PRODUCTS, REMOVE_PRODUCT } from "../actions/products";

// - state: stato che arriva dallo store. 
// - action: quella che abbiamo creato noi dandola al dispatch. 
export default function productsReducer(state = [], action) {
  switch (action.type) {
    case ADD_PRODUCTS:
      return [
        ...state,
        ...action.products,
      ];
    case REMOVE_PRODUCT:
      return [...state].filter(p => p.id != action.productId);
    default:
      return state;
  }
}
