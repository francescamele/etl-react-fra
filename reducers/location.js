import { ANIMATE_COORDS, CHANGE_COORDS, INIT_COORDS, LOCK_MARKER, PERMISSION_CHECK } from "../actions/location";

export default function locationReducer(state = [], action) {
    // per farvela semplice: controllo. (!!!!!)
    // 1. Scateni due azioni: a. Inizio controllo; quando a è finito, tipo boolean: 
    //   ho i permessi? false. b. Fine controllo -> ho i permessi (???)
    // 2. Inizio chiamata -> ho una chiave che mi dice true?
    //   poi ???
    // Soluzione più facile: action controllo. Richiedo i permessi, e la action 
    // setta se ho i permessi sulla base di quello che mi arriva dalla funzione if
    switch (action.type) {
        case PERMISSION_CHECK:
            return [
                ...state,
                ...action.check
            ];
        case INIT_COORDS:
            return [];
        case CHANGE_COORDS:
            return [];
        case ANIMATE_COORDS:
            return []
        case LOCK_MARKER:
            return [];
        default:
            return state;
    }
}