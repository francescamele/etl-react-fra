import { Rating } from "./rating";

export class Product {
    constructor(
        id, 
        title, 
        image,
        price,
        category, 
        description
    ) {
        this.category = category;
        this.description = description;
        this.id = id;
        this.image = image;
        this.price = price;
        this.rating = Rating;
        this.title = title;
    }
}