export const buttons = {
    backgroundColor: '#1296FA',
    borderRadius: 30,
    bottom: 10,
    padding: 20,
    position: 'absolute',
    right: 10,
};

export const buttonActive = Object.assign({}, buttons, {backgroundColor: 'powderblue'});