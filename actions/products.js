export const ADD_PRODUCTS = "ADD_PRODUCTS";
export const REMOVE_PRODUCT = "REMOVE_PRODUCT";

export const addProducts = products => {
  return {
    products,
    type: ADD_PRODUCTS,
  };
};

export const removeProduct = productId => {
  return {
    productId,
    type: REMOVE_PRODUCT,
  };
};
