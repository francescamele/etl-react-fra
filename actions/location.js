export const ANIMATE_COORDS = "ANIMATE_COORDS";
export const CHANGE_COORDS = "CHANGE_COORDS";
export const INIT_COORDS = "INIT_COORDS";
export const LOCK_MARKER = "LOCK_MARKER";
export const PERMISSION_CHECK = "PERMISSION_CHECK";

export const animateCoords = coords => ({
    coords,
    type: ANIMATE_COORDS
});

export const changeCoords = coords => ({
    coords,
    type: CHANGE_COORDS
});

export const initialiseCoords = coords => ({
    coords,
    type: INIT_COORDS
});

export const lockMarker = toggle => ({
    toggle,
    type: LOCK_MARKER
});

export const checkPermission = check => ({
    check,
    type: PERMISSION_CHECK
});