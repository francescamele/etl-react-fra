module.exports = function(api) {
  api.cache(true);
  return {
    plugins: [
      // Volendo, puoi anche non mettere le [], se non c'è un oggetto di 
      // configurazione (come con presets):
      '@babel/plugin-transform-flow-strip-types',
      [
        // Nome del plugin:
        '@babel/plugin-proposal-private-methods', 
        // Oggetto di configurazione:
        {
          "loose": true,
          "shippedProposals": true
        }
      ]
    ],
    presets: [
      'babel-preset-expo'
    ],
  };
};
