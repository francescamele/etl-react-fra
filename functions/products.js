import axios from "axios";
import { Product } from "../models/product";
import { Rating } from "../models/rating";

export const getProducts = async () => {
    try {
        const products = await axios.get('https://fakestoreapi.com/products');

        return products.data.map(p => new Product(
                p.id,
                p.title,
                p.image,
                p.price,
                p.category,
                p.rating,
                p.description,
                new Rating(p.rating.rate, p.rating.count)
            )
        );
    } catch(e) {
        console.error(e);
        return [];
    }
}

export const getProduct = async (id) => {
    try {
        const product = await axios.get(`https://fakestoreapi.com/products/${id}`);

        return new Product(
            product.data.id,
            product.data.title,
            product.data.image,
            product.data.price,
            product.data.category,
            product.data.rating,
            product.data.description,
            new Rating(product.data.rating.rate, product.data.rating.count)
        );
    } catch(e) {
        console.error(e);
        return [];
    }
}