import * as Location from 'expo-location';

// Function EXPRESSION: 
export const checkGeolocationPermissions = async () => {
    // getAsync => controlla se ci sono i permessi che gli passi
    // const { status } = await getAsync(LOCATION_FOREGROUND);

    // 21.02: ci dice se abbiamo il permesso o no
    // Location.getForegroundPermissionsAsync();
    // askAsync: ti dice se ci sono in relazione alla richiesta all'utente: 
    // se li abbiamo, non li chiedo - se NON li abbiamo, li chiedo. Questo 
    // è di expo-permissions: invece adesso, con expo-location, ho: 
    // const { status } = await Location.requestForegroundPermissionsAsync();

    // return status === 'granted';

    // 21.02 32': Posso fare: 
    const { granted } = await Location.requestForegroundPermissionsAsync();

    return granted;
}