import React from "react";
import { Image, Pressable, StyleSheet, Text, View } from "react-native";
import { getProduct } from "../../functions/products";

export default class ProductComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            product: {},
            message: false
        }
    }

    async componentDidMount() {
        // Prende una promise: mi ci aggancio con async
        this.setState({
            product: await getProduct(this.props.route.params.id),
        });
    }

    render() {
        console.log('render');
        // Questa va in questo metodo perch* viene rieseguita ogni volta 
        // che viene rieseguito il metodo render
        const v = this.state.message === true ? <View><Text>What a beautiful product!</Text></View> : null;
        
        return (
            <View>
                <View style={style.row}>
                    <View style={style.oneThird}>
                        <Image source={ {uri: this.state.product.image} } style={style.image} />
                    </View>
                    <View style={style.twoThird}>
                        <Pressable onPress={() => {
                                console.log('you clicked me');
                                return this.toggleMessage();
                            }}>
                            <Text>{this.state.product.title}</Text>
                        </Pressable>
                    </View>
                </View>
                {v}
            </View>
        )
    }

    toggleMessage() {
        this.setState({
            message: !this.state.message
        })
    }
}

const style = StyleSheet.create({
    image: {
        height: 100,
        width: 100
    },
    oneThird: {
        flex: 0.33
    },
    twoThird: {
        flex: 0.66
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    }
});