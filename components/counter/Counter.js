import { useState } from "react";
import { Button, Pressable, StyleSheet, Text, View } from "react-native";

export function Counter(props) {
    // Inizialmente:
    // const s = useState(0);

    const [cnt, updateFn] = useState(props.start || 0);

    const doTheMath = (isSum) => {
        // Com'era prima:
        // cnt++;
        // console.log(cnt);

        // Poi, invece di fare:
        // s[1]();
        // Faccio:
        // updateFn(v => v + 1);

        // if (isSum) {
        //     updateFn(v => v + 1);
        // } else {
        //     updateFn(v => v - 1);
        // }

        updateFn(cnt => isSum ? cnt + 1 : cnt - 1);
    }
    // let cnt = 0;

    // const subtract = () => updateFn(v = v - 1);

    return (
        <View>
            <Text style={styles.text}>{cnt}</Text>
            <View style={[styles.container]}>
                <Pressable style={styles.pressable} onPress={() => doTheMath(true)}>
                    <Text style={styles.stoca}>Add one</Text> 
                </Pressable>
                <Pressable style={styles.pressable} onPress={() => doTheMath(false)}>
                    <Text style={styles.stoca}>Subtract one</Text> 
                </Pressable> 
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    pressable: {
        padding: 20,
    },
    stoca: {
        width: 100,
        backgroundColor: 'powderblue',
        height: 100,
        fontSize: 20,
        textAlign: 'center',
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        paddingBottom: 30,
    }
})