import React from "react";
import { Pressable, Text, View } from "react-native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ProductsComponent from "../products/Products";
import ProductComponent from "../product/Product";

const Stack = createNativeStackNavigator();

export default class HomeComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Stack.Navigator>
                    <Stack.Screen name='Products' component={ProductsComponent} />
                    <Stack.Screen name='Product' component={ProductComponent} />
                </Stack.Navigator>
                <Text>Benvenuto! bella zi'</Text>
                <Pressable onPress={() => this.props.navigation.navigate('Products')}>
                    <Text>Prodotti</Text>
                </Pressable>
            </View>
        );
    }
}