import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";

// redux
import { connect } from "react-redux";

// react native elements
import { FlatList, Image, Pressable, StyleSheet, Text, View } from "react-native";

// functions
import { getProducts } from "../../functions/products";

// actions
import { addProducts } from "../../actions/products";

const Stack = createNativeStackNavigator();

const style = StyleSheet.create({
    image: {
        height: 100,
        width: 100
    },
    oneThird: {
        flex: 0.33
    },
    twoThird: {
        flex: 0.66
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    }
});

// Levo l'export: nessuno si deve prendere questo comp, ma solo lo stato:
class ProductsComponent extends React.Component {
    #_keyExtractor(product) {
        return product.id.toString();
    }

    // renderItem è un metodo: "#_renderItem({item}) " è una firma. 
    // {item} è un param formale: non è "item", 
    #_renderItem = ({item}) => {
        return (
            <View>
            <View style={style.row}>
                <View style={style.oneThird}>
                    <Image source={ {uri: item.image} } style={style.image} />
                </View>
                <View style={style.twoThird}>
                    <Pressable onPress={() => this.props.navigation.navigate('Product', {id: item.id})}>
                        <Text>{item.title}</Text>
                    </Pressable>
                </View>
                {/* <View style={style.oneThird}>
                    <Image source={ {uri: item.image} } style={style.image} />
                </View> */}
            </View>
            </View>
        );
    }

    // Con redux si può togliere this.state, e a quel punto anche tutto il constructor

    async componentDidMount() {
        this.props.addProducts(await getProducts());
    }

    render() {
        return (
            <View>
                {/* <Stack.Navigator>
                    <Stack.Screen name='Product' component={ProductsComponent} />
                </Stack.Navigator> */}
                <FlatList data={this.props.products} keyExtractor={this.#_keyExtractor} renderItem={this.#_renderItem}></FlatList>
            </View>
        );
    }
}

const mapDispatchToProps = { addProducts };
const mapStateToProps = state => ({
    products: state.products
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductsComponent);