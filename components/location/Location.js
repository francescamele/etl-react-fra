import React from "react";
import { Button, Dimensions, Platform, Pressable, StyleSheet, Text, View } from "react-native";
import MapView, { Marker } from "react-native-maps";
import { checkGeolocationPermissions } from "../../functions/location";
import * as Location from 'expo-location';
import { buttonActive, buttons } from "../../elements/buttons";

export default class LocationComponent extends React.Component {
    constructor(props) {
        super(props);

        this.map = React.createRef();
        this.marker = React.createRef();

        // Lo state causa il render: forse non serve scriverla qui, la prop booleana, 
        // ma Andrea lo preferisce così per 1h57 22.02
        this.state = {
            isMapLocked: false,
            latitude: 0,
            longitude: 0,
            latitudeD: 0.012,
            longitudeD: 0.012
        }
    }

    async componentDidMount() {
        let hasPermission;

        try {
            hasPermission = await checkGeolocationPermissions();
        } catch(e) {
            console.error(e);
        }

        // Tronco la piramide sul nascere: metto la LOGICA SULLA 
        // LINEA PRINCIPALE DEL METODO DELLA FUNZIONE, invece che 
        // all'interno della piramide (cioè nell'if)
        if (!hasPermission) {
            console.log('OPS');
            return;
        }

        // Questa fn vuole un oggetto di configurazione: se non sai che 
        // passargli, gliene passi uno vuoto
        Location.watchPositionAsync({
            accuracy: 6,
            distanceInterval: 1
        }, p => {
            // Per bloccare l'errore del null this2.map.current.animateCamera:
            if (!this.map.current || !this.marker.current) {
                return;
            }

            // console.log(p);
            // Costante nello scope su questo elemento:
            const center = {
                latitude: p.coords.latitude,
                longitude: p.coords.longitude,
                latitudeDelta: p.coords.l
            };

            if (!this.state.isMapLocked) {   
                this.map.current.animateCamera({
                    center
                }, {
                    duration: 1000
                });
            }

            if (Platform.OS === 'android') {
                this.marker.current.animateMarkerToCoordinate(center, 200);
            } else {
                this.marker.current.coordinate = center;
            }

            // this.marker.current.redraw();
            // this.marker.current.animateMarkerToCoordinate(center, 200);

            // Il setState viene eseguito schedulato: se lo scrivi qui con nuovi 
            // argomenti di lat e long, l'aggiornamento non sarà già stato fatto 
            // quando arrivi a this.map.current: se scheduli un setState, sicuramente 
            // verrà eseguito DOPO il map.current
            this.setState(center);
        });
        // Qui interroghiamo l'ogg navigator che ci permette di 
        // 08
        // watchPosition() è un metodo che ci dà la posizione: non 
        // me la dà come risultato
        // ...
        // Sull'evento click, davi la funzione da eseguire:
        // navigator.geolocation.watchPosition(p => console.log(p));
    }

    render() {
        // console.log(a);
        return (
            <View>
                {/* Dobbiamo mostrare quello che arriva da Location.watchPositinoAsync: */}
                {/* <Text>Lat: { this.state.latitude }</Text>
                <Text>Long: { this.state.longitude }</Text> */}
                <MapView
                    initialRegion={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: this.state.latitudeD,
                        longitudeDelta: this.state.longitudeD,
                    }}
                    ref={this.map}
                    style={style.map} 
                >
                    <Marker
                        coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }}
                        ref={this.marker}
                    ></Marker>
                </MapView>
                <Pressable style={style.cazzi}>
                    <Text>Cazziiii</Text>
                </Pressable>
                <Pressable style={style.aru}>
                    <Text>Aruuu</Text>
                </Pressable>
                {/* <Button style={style.btn} title="Marker"></Button> */}
                <Pressable style={this.state.isMapLocked ? style.mapLockBtn : style.mapLockBtnActive} onPress={() => this.toggleMapLock()}>
                    <Text style={style.text}>{ this.state.isMapLocked ? 'Is it just Fanta-sea?' : 'Is this the real life?'}</Text>
                </Pressable>
                {/* <Text style={style.debug}>{ this.state.latitude } { this.state.longitude }</Text> */}
            </View>
        );
    }

    toggleMapLock() {
        // L'unica cosa che devo fare è un setState: devo dire che isMapLocked è:
        this.setState({
            isMapLocked: !this.state.isMapLocked
        });
    }

    toggleAru() {
        this.state.latitudeD = this.state.latitudeD - 0.1;
        this.state.longitudeD = this.state.longitudeD - 0.1;
    }

    toggleCazzi() {
        this.state.latitudeD = this.state.latitudeD + 0.1;
        this.state.longitudeD = this.state.longitudeD + 0.1;
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    aru: {
        backgroundColor: '#1296FA',
        borderRadius: 15,
        bottom: 10,
        padding: 20,
        position: 'absolute',
        left: 10,
    },
    cazzi: {
        backgroundColor: '#1296FA',
        borderRadius: 15,
        bottom: 80,
        padding: 20,
        position: 'absolute',
        left: 10,
    },
    map: {
        // width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        height: 650,
        width: Dimensions.get('window').height,
    },
    mapLockBtn: buttons,
    mapLockBtnActive: buttonActive,
    mapLockBtnText: {
        color: 'black'
    }
});