import React from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";

export default class CounterCl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cnt: props.start || 0
        };
    }

    doTheMath(isSum) {
        this.setState({
            cnt: isSum ? this.state.cnt + 1 : this.state.cnt -1
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>{ this.state.cnt }</Text>
                <Pressable onPress={() => this.doTheMath(true)}>
                    <Text>Add one</Text>
                </Pressable>
                <Pressable onPress={() => this.doTheMath(false)}>
                    <Text>Subtract one</Text>
                </Pressable>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        fontSize: 20,
        position: 'absolute',
        bottom: 200
    }
})